/**
 * Логика сортировки элементов
 */

/** Небольшие настройки сортировки */
var MOVER_SENSITIVITY = 2 // Чувствительность перестановки элементов
var RESTRICT_VERTICAL_OVERLAP = false // Ограничить вертикальное перемещение
var RESTRICT_X_AXIS = false // Ограничить перемещение по оси X
var RESTRICT_Y_AXIS = false // Ограничить перемещение по оси Y

/** Константы обозначения классов */
const SORTABLE_LIST = 'sorting-list'
const SORTABLE_LIST_ITEM = 'sorting-list__item'
const ITEM_CONTENT = 'item__content'
const ITEM_CONTENT_MOVE = 'item-content__move'
const ITEM_CONTENT_TEXT = 'item-content__text'
const ITEM_CONTENT_REMOVE = 'item-content__remove'

/** Объект данных о перетискивании */
var SortingList = {
    listItem: null,
    item: null,
    dragItem: null,
    dragOffset: {},
}

/**
 * Получение координат мыши/касания
 */
function getClientCoordinates() {
    const isMouse = window.event.type.toLowerCase().includes('mouse')

    return [
        isMouse ? window.event.clientX : window.event.changedTouches[0].pageX,
        isMouse ? window.event.clientY : window.event.changedTouches[0].pageY
    ]
}

/**
 * Создание нового элемента в список
 */
function createNewElement(text) {
    let item = document.createElement('div')
    let itemContent = document.createElement('div')
    let itemContentMove = document.createElement('div')
    let itemContentText = document.createElement('div')
    let itemContentRemove = document.createElement('div')
    let itemTextSpan = document.createElement('span')

    item.className = SORTABLE_LIST_ITEM
    itemContent.className = ITEM_CONTENT
    itemContentMove.className = ITEM_CONTENT_MOVE
    itemContentText.className = ITEM_CONTENT_TEXT
    itemContentRemove.className = ITEM_CONTENT_REMOVE

    itemTextSpan.innerText = text
    itemContentText.append(itemTextSpan)
    itemContentMove.setAttribute('title', 'Переместить')
    itemContentRemove.setAttribute('title', 'Удалить')

    // itemContent.style.cursor = 'move'
    itemContent.append(itemContentMove)
    itemContent.append(itemContentText)
    itemContent.append(itemContentRemove)

    item.append(itemContent)

    return item
}

/**
 * Создание нового элемента
 */
function createDraggableElement(elem) {
    const elemRect = elem.getBoundingClientRect()

    const newElem = document.createElement('div')

    newElem.innerHTML = elem.innerHTML
    newElem.className = `${SORTABLE_LIST_ITEM} ${SORTABLE_LIST_ITEM}--dragging`
    newElem.style.width = `${elem.offsetWidth}px`
    newElem.style.height = `${elem.offsetHeight}px`
    newElem.style.left = `${elemRect.left}px`
    newElem.style.top = `${elemRect.top}px`

    document.body.appendChild(newElem)

    SortingList.listItem = elem.closest(`.${SORTABLE_LIST}`)
    SortingList.item = elem
    SortingList.dragItem = newElem

    let [ clientX, clientY ] = getClientCoordinates()

    SortingList.dragOffset.x = clientX - elemRect.left
    SortingList.dragOffset.y = clientY - elemRect.top

    return newElem
}

/**
 * Начало передвижения элемента
 */
function onElementMouseDown(event) {
    let target = event.target

    if (!target.classList.contains(SORTABLE_LIST_ITEM)) {
        target = target.closest(`.${SORTABLE_LIST_ITEM}`)
    }

    createDraggableElement(target)

    target.style.visibility = 'hidden'
}

/**
 * Удаление элемента из списка
 */
function removeElementFromList(elem) {
    const item = elem.target.closest(`.${SORTABLE_LIST_ITEM}`)

    if (item) {
        const itemIndex = sortingItems.indexOf(item)

        item.remove()
        sortingItems.splice(itemIndex, 1)

        setKeyValue('count', sortingItems.length)
    }
}

/**
 * Перемещение элемента
 */
function onMouseMoveElement(event) {
    event.preventDefault()

    if (!SortingList.dragItem) return

    const [ clientX, clientY ] = getClientCoordinates()
    const listRect = SortingList.listItem.getBoundingClientRect()

    const itemIndex = sortingItems.indexOf(SortingList.item)
    const dragItemTop = clientY - SortingList.dragOffset.y

    /** Выход за верхнюю границу */
    if (dragItemTop <= listRect.top) {
        if (itemIndex != 0) {
            sortingList.prepend(SortingList.item)
        }

        if (RESTRICT_VERTICAL_OVERLAP) {
            SortingList.dragItem.style.top = `${listRect.top}px`

            return
        }
    }

    /** Выход за нижнюю границу */
    if (dragItemTop + SortingList.dragItem.offsetHeight >= listRect.bottom) {
        if (itemIndex != sortingItems.length) {
            sortingList.appendChild(SortingList.item)
        }

        if (RESTRICT_VERTICAL_OVERLAP) {
            let bottomPosition = listRect.bottom - SortingList.dragItem.offsetHeight

            SortingList.dragItem.style.top =`${bottomPosition}px`

            return
        }
    }

    /** Перемещение по двум осям/ограничение перемещения */
    if (!RESTRICT_X_AXIS) {
        SortingList.dragItem.style.left = `${clientX - SortingList.dragOffset.x}px`
    }

    if (!RESTRICT_Y_AXIS) {
        SortingList.dragItem.style.top = `${clientY - SortingList.dragOffset.y}px`
    }

    let moverOutOfItem = false
    const moverRect = SortingList.dragItem.getBoundingClientRect()
    const itemRect = SortingList.item.getBoundingClientRect()

    if (moverRect.top > itemRect.bottom ||
        moverRect.bottom < itemRect.top) {
        moverOutOfItem = true
    }

    /** Проверка пересечения других элементов */
    sortingItems.forEach(function(elem) {
        if (elem === SortingList.item) return

        const elemRect = elem.getBoundingClientRect()

        let elemTop = elemRect.top - MOVER_SENSITIVITY
        let elemBottom = elemRect.bottom + MOVER_SENSITIVITY

        if (moverOutOfItem) {
            let diff = (elemRect.bottom - elemRect.top) / 2
            elemTop -= diff
            elemBottom += diff
        }

        if (moverRect.top >= elemTop &&
            moverRect.bottom <= elemBottom) {
            const elemIndex = sortingItems.indexOf(elem)

            if (elemIndex > itemIndex) {
                elem.after(SortingList.item)
            }
            else {
                elem.before(SortingList.item)
            }

            [
                sortingItems[elemIndex],
                sortingItems[itemIndex],
            ] = [
                sortingItems[itemIndex],
                sortingItems[elemIndex],
            ]
        }
    })
}

/**
 * Завершение передвижения элемента
 */
function onMouseUpElement() {
    if (!SortingList.dragItem) return

    SortingList.dragItem.remove()
    SortingList.dragItem = null

    SortingList.item.style.visibility = null
    SortingList.item = null
}

/**
 * Добавление нового элемента в список
 */
function appendElementToList(elem) {
    const itemMove = elem.querySelector(`.${ITEM_CONTENT_MOVE}`) || elem
    const itemRemove = elem.querySelector(`.${ITEM_CONTENT_REMOVE}`)

    itemMove.addEventListener('mousedown', onElementMouseDown)
    itemMove.addEventListener('touchstart', onElementMouseDown)

    if (itemRemove) {
        itemRemove.addEventListener('click', removeElementFromList)
    }

    sortingList.appendChild(elem)
    sortingItems.push(elem)
}

/** Дополнительные события для контроля перетаскивания */
document.addEventListener('mousemove', onMouseMoveElement)
document.addEventListener('touchmove', onMouseMoveElement)
document.addEventListener('mouseup', onMouseUpElement)
document.addEventListener('touchend', onMouseUpElement)

/** Отбор элементов */
var sortingList = document.querySelector('.sorting-list')
var sortingItems = Array.from(document.getElementsByClassName(SORTABLE_LIST_ITEM))
