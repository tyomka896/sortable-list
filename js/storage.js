/**
 * Восстановление предыдущего состояния
 */

if (getKeyValue('x')) {
    document.getElementById('axis-x').click()
}

if (getKeyValue('y')) {
    document.getElementById('axis-y').click()
}

if (getKeyValue('b')) {
    document.getElementById('restrict-border').click()
}

/**
 * Популяция элементов
 */
if (sortingList) {
    let count = getKeyValue('count') || 5

    Array(count).fill(0).map(function(_, index) {
        let item = createNewElement(`Item ${index + 1}`)

        item.setAttribute('item', index + 1)

        return {
            item: item,
            random: Math.random(),
        }
    })
        .sort(function(a, b) { return a.random - b.random })
        .forEach(function(elem) { appendElementToList(elem.item) })
}
