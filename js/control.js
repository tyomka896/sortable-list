/**
 * Управление элементами и дополнительными настройками сортировки
 */

var LOCAL_STORAGE = 'settings'

/**
* Получение значения ключа в хранилище
 */
function getKeyValue(key) {
    let ls = JSON.parse(localStorage.getItem(LOCAL_STORAGE)) || {}

    return ls[key]
}

/**
* Обновление значения ключа в хранилище
 */
function setKeyValue(key, value) {
    let ls = JSON.parse(localStorage.getItem(LOCAL_STORAGE)) || {}

    ls[key] = value

    localStorage.setItem(LOCAL_STORAGE, JSON.stringify(ls))
}

/**
 * Ограничить по оси X
 */
document.getElementById('axis-x').addEventListener('click', function(event) {
    setKeyValue('x', !RESTRICT_X_AXIS)

    RESTRICT_X_AXIS = !RESTRICT_X_AXIS

    if (RESTRICT_X_AXIS) {
        event.target.classList.add('one')

        if (RESTRICT_Y_AXIS) {
            document.getElementById('axis-y').click()
        }
    }
    else {
        event.target.classList.remove('one')
    }
})

/**
 * Ограничить по оси Y
 */
document.getElementById('axis-y').addEventListener('click', function(event) {
    setKeyValue('y', !RESTRICT_Y_AXIS)

    RESTRICT_Y_AXIS = !RESTRICT_Y_AXIS

    if (RESTRICT_Y_AXIS) {
        event.target.classList.add('one')

        if (RESTRICT_X_AXIS) {
            document.getElementById('axis-x').click()
        }
    }
    else {
        event.target.classList.remove('one')
    }
})

/**
 * Перемещение в пределах границ
 */
document.getElementById('restrict-border').addEventListener('click', function(event) {
    setKeyValue('b', !RESTRICT_VERTICAL_OVERLAP)

    RESTRICT_VERTICAL_OVERLAP = !RESTRICT_VERTICAL_OVERLAP

    if (RESTRICT_VERTICAL_OVERLAP) {
        event.target.classList.add('two')
    }
    else {
        event.target.classList.remove('two')
    }
})

/**
 * Добавление новой строки
 */
document.getElementById('add-row').addEventListener('click', function() {
    const count = sortingItems.length

    const item = createNewElement(`Item ${count + 1}`)
    item.setAttribute('item', count + 1)

    appendElementToList(item)
    window.scrollTo(0,document.body.scrollHeight)

    setKeyValue('count', sortingItems.length)
})

/**
 * Отслеживание движения мыши
 */
document.addEventListener('mousemove', function(event) {
    if (event.shiftKey && event.ctrlKey) {
        console.log(`mouseX ${event.clientX} | mouseY ${event.clientY}`)
    }
})
